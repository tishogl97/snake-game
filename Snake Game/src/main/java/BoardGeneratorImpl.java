import com.googlecode.lanterna.graphics.TextGraphics;
import contracts.BoardGenerator;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class BoardGeneratorImpl implements BoardGenerator {

    private TextGraphics txtGraphics;
    private String boardName;

    public BoardGeneratorImpl(String boardName, TextGraphics txtGraphics) {
        this.boardName = boardName;
        this.txtGraphics = txtGraphics;

    }

    @Override
    public char[][] boardGeneration() throws FileNotFoundException {
        try (BufferedReader br = Files.newBufferedReader(Paths.get(boardName))) {
            int width = Integer.parseInt(br.readLine());
            int height = Integer.parseInt(br.readLine());
            char[][] board = new char[height][width];
            for (int rows = 0; rows < height; rows++) {
                char[] pixels = br.readLine().toCharArray();
                for (int cols = 0; cols < width; cols++) {
                    board[rows][cols] = pixels[cols];
                    if(pixels[cols] == '#') {
                        txtGraphics.putString(cols, rows, "#");
                    }
                }
            }
            return board;
        } catch (IOException e) {
            throw new FileNotFoundException();
        }
    }
}

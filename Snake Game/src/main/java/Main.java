import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.screen.Screen;
import com.googlecode.lanterna.screen.TerminalScreen;
import com.googlecode.lanterna.terminal.DefaultTerminalFactory;
import com.googlecode.lanterna.terminal.Terminal;
import contracts.*;

import java.io.IOException;
import java.sql.SQLOutput;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner in = new Scanner(System.in);
        System.out.print("Choose the board you want to play \n(There is only 1 right now which is \"board1\")\nAfter the terminal opens press any arrow key to start\n Board Name: ");
        String board = in.nextLine();
        Terminal terminal = new DefaultTerminalFactory().createTerminal();
        Screen screen = new TerminalScreen(terminal);
        TextGraphics txtGraphics = screen.newTextGraphics();
        BoardGenerator bg = new BoardGeneratorImpl(board, txtGraphics);
        SnakeGenerator sg = new SnakeGeneratorImpl(txtGraphics);
        FruitGenerator fg = new FruitGeneratorImpl(txtGraphics);
        Movement movement = new MovementImpl(txtGraphics, sg, screen, bg, fg);
        screen.startScreen();

        movement.startPlaying();

        screen.refresh();
    }
}

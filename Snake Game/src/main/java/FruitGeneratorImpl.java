import com.googlecode.lanterna.graphics.TextGraphics;
import contracts.FruitGenerator;

public class FruitGeneratorImpl implements FruitGenerator {
    private TextGraphics textGraphics;

    public FruitGeneratorImpl(TextGraphics textGraphics) {
        this.textGraphics = textGraphics;
    }

    public int[] createFruit(int maxRows, int maxCols, char[][] board, boolean isApple) {
            int randomRow = (int) (Math.random() *((maxRows - 1) + 1) + 0);
            int randomCol = (int) (Math.random() * ((maxCols - 1) + 1) + 0);
            while(board[randomRow][randomCol] != ' ') {
                randomRow = (int) (Math.random() * ((maxRows - 1) + 1) + 0);
                randomCol = (int) (Math.random() * ((maxCols - 1) + 1) + 0);
            }
            if(isApple) {
                board[randomRow][randomCol] = 'o';
                textGraphics.putString(randomCol, randomRow, "o");
            } else {
                board[randomRow][randomCol] = 'd';
                textGraphics.putString(randomCol, randomRow, "d");
            }
        return new int[] {randomRow, randomCol};
    }
    public void removeFruit(int row, int col, char[][] board) {
        board[row][col] = ' ';
        textGraphics.putString(col, row, " ");
    }
}

import com.googlecode.lanterna.graphics.TextGraphics;
import contracts.SnakeGenerator;

import java.util.ArrayList;

public class SnakeGeneratorImpl implements SnakeGenerator {
    private static final int HEAD_START_ROW = 6;
    private static final int HEAD_START_COL = 6;
    private static final int MIDDLE_START_ROW = 6;
    private static final int MIDDLE_START_COL = 5;
    private static final int TAIL_START_ROW = 6;
    private static final int TAIL_START_COL = 4;
    private TextGraphics txtGraphics;


    public SnakeGeneratorImpl(TextGraphics txtGraphics) {
        this.txtGraphics = txtGraphics;
    }

    @Override
    public ArrayList<ArrayList<Integer>> createSnake() {
        ArrayList<Integer> head = new ArrayList<>();
        head.add(HEAD_START_ROW);
        head.add(HEAD_START_COL);
        ArrayList<Integer> middle = new ArrayList<>();
        middle.add(MIDDLE_START_ROW);
        middle.add(MIDDLE_START_COL);
        ArrayList<Integer> tail = new ArrayList<>();
        tail.add(TAIL_START_ROW);
        tail.add(TAIL_START_COL);
        ArrayList<ArrayList<Integer>> snake = new ArrayList<>();
        snake.add(head);
        snake.add(middle);
        snake.add(tail);
        return snake;
    }

    @Override
    public void displaySnake() {
        txtGraphics.putString(HEAD_START_COL, HEAD_START_ROW, "*");
        txtGraphics.putString(MIDDLE_START_COL,MIDDLE_START_ROW, "*");
        txtGraphics.putString(TAIL_START_COL, TAIL_START_ROW, "*");
    }
}

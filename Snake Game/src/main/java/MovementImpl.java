import com.googlecode.lanterna.graphics.TextGraphics;
import com.googlecode.lanterna.screen.Screen;
import contracts.BoardGenerator;
import contracts.FruitGenerator;
import contracts.Movement;
import contracts.SnakeGenerator;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;

public class MovementImpl implements Movement {

    private boolean up;
    private boolean down;
    private boolean right;
    private boolean left;
    private int boardWidth;
    private int boardHeight;
    private int moves;
    private TextGraphics txtGraphics;
    private SnakeGenerator snakeGenerator;
    private Screen screen;
    private BoardGenerator boardGenerator;
    private FruitGenerator fruitGenerator;

    public MovementImpl(TextGraphics txtGraphics, SnakeGenerator snakeGenerator, Screen screen, BoardGenerator boardGenerator, FruitGenerator fruitGenerator) {
        this.txtGraphics = txtGraphics;
        this.snakeGenerator = snakeGenerator;
        this.screen = screen;
        this.boardGenerator = boardGenerator;
        this.fruitGenerator = fruitGenerator;
    }

    @Override
    public void startPlaying() {
        try {
            screen.clear();
            boolean ateApple = false;
            boolean atePear = false;
            boolean gameOver = true;
            int fruitNumber = 0;
            moves = 0;
            // Generates the board on the terminal and as a matrix of characters
            char[][] board = boardGenerator.boardGeneration();
            boardWidth = board[0].length;
            boardHeight = board.length;
            ArrayList<ArrayList<Integer>> snake = snakeGenerator.createSnake();
            snakeGenerator.displaySnake();
            int[] fruitCoordinates = fruitGenerator.createFruit(boardHeight, boardWidth, board, true);
            board[snake.get(0).get(0)][snake.get(0).get(1)] = '*';
            board[snake.get(1).get(0)][snake.get(1).get(1)] = '*';
            board[snake.get(2).get(0)][snake.get(2).get(1)] = '*';
            while (gameOver) {
                // If you have made 10 moves it changes the fruit position or if 5 fruits were spawned put a pear
                if (fruitNumber == 5) {
                    fruitGenerator.removeFruit(fruitCoordinates[0], fruitCoordinates[1], board);
                    fruitCoordinates = fruitGenerator.createFruit(boardHeight, boardWidth, board, false);
                    fruitNumber = 0;
                    moves = 0;
                }
                if (moves == 10) {
                    fruitGenerator.removeFruit(fruitCoordinates[0], fruitCoordinates[1], board);
                    fruitCoordinates = fruitGenerator.createFruit(boardHeight, boardWidth, board, true);
                    moves = 0;
                    fruitNumber = fruitNumber + 1;
                }

                // Saves where the Head was, so the next element can come on this position
                int previousRow = snake.get(0).get(0);
                int previousCol = snake.get(0).get(1);
                board[previousRow][previousCol] = ' ';
                // Checks for pressed key and if the direction is valid
                KeyEventDispatcher dispatcher = e -> {
                    if (e.getKeyCode() == KeyEvent.VK_UP && e.getID() == KeyEvent.KEY_PRESSED && !up && !down) {

                        up = true;
                        down = false;
                        right = false;
                        left = false;
                        moves++;

                    }
                    if (e.getKeyCode() == KeyEvent.VK_DOWN && e.getID() == KeyEvent.KEY_PRESSED && !down && !up) {

                        up = false;
                        down = true;
                        right = false;
                        left = false;
                        moves++;

                    }
                    if (e.getKeyCode() == KeyEvent.VK_RIGHT && e.getID() == KeyEvent.KEY_PRESSED && !right && !left) {

                        up = false;
                        down = false;
                        right = true;
                        left = false;
                        moves++;

                    }
                    if (e.getKeyCode() == KeyEvent.VK_LEFT && e.getID() == KeyEvent.KEY_PRESSED && !left && !right) {

                        up = false;
                        down = false;
                        right = false;
                        left = true;
                        moves++;
                    }
                    return false;
                };
                KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(dispatcher);
                Thread.sleep(300);

                // Depending on the pressed button makes a move (if the button is invalid uses the previous direction)
                if (right) {
                    // Snake's head position moves 1 column to the right
                    snake.get(0).set(1, snake.get(0).get(1) + 1);
                    atePear = checkForPear(snake.get(0).get(0), snake.get(0).get(1), board);
                    gameOver = checkValidMove(snake.get(0).get(0), snake.get(0).get(1), board);
                    ateApple = checkForApple(snake.get(0).get(0), snake.get(0).get(1), board);
                    board[snake.get(0).get(0)][snake.get(0).get(1)] = '*';
                    txtGraphics.putString(snake.get(0).get(1), snake.get(0).get(0), "*");
                } else if (up) {
                    // Snake's head moves one row up
                    snake.get(0).set(0, snake.get(0).get(0) - 1);
                    atePear = checkForPear(snake.get(0).get(0), snake.get(0).get(1), board);
                    gameOver = checkValidMove(snake.get(0).get(0), snake.get(0).get(1), board);
                    ateApple = checkForApple(snake.get(0).get(0), snake.get(0).get(1), board);
                    board[snake.get(0).get(0)][snake.get(0).get(1)] = '*';
                    txtGraphics.putString(snake.get(0).get(1), snake.get(0).get(0), "*");
                } else if (down) {
                    // Snake's head moves on row down
                    snake.get(0).set(0, snake.get(0).get(0) + 1);
                    atePear = checkForPear(snake.get(0).get(0), snake.get(0).get(1), board);
                    gameOver = checkValidMove(snake.get(0).get(0), snake.get(0).get(1), board);
                    ateApple = checkForApple(snake.get(0).get(0), snake.get(0).get(1), board);
                    board[snake.get(0).get(0)][snake.get(0).get(1)] = '*';
                    txtGraphics.putString(snake.get(0).get(1), snake.get(0).get(0), "*");
                } else if (left) {
                    // Snake's head moves one column to the left
                    snake.get(0).set(1, snake.get(0).get(1) - 1);
                    atePear = checkForPear(snake.get(0).get(0), snake.get(0).get(1), board);
                    gameOver = checkValidMove(snake.get(0).get(0), snake.get(0).get(1), board);
                    ateApple = checkForApple(snake.get(0).get(0), snake.get(0).get(1), board);
                    board[snake.get(0).get(0)][snake.get(0).get(1)] = '*';
                    txtGraphics.putString(snake.get(0).get(1), snake.get(0).get(0), "*");
                }

                for (int element = 1; element < snake.size(); element++) {
                    // Saves the current element position, so we can use it and put the next one there
                    int currentRow = snake.get(element).get(0);
                    int currentCol = snake.get(element).get(1);
                    //Changes the the element position and puts it where the previous was
                    snake.get(element).set(0, previousRow);
                    snake.get(element).set(1, previousCol);

                    // Creates mark on the board, that there is snake part and displays the part on the terminal
                    board[snake.get(element).get(0)][snake.get(element).get(1)] = '*';
                    txtGraphics.putString(snake.get(element).get(1), snake.get(element).get(0), "*");
                    // Clears the cell after the element has been moved
                    board[currentRow][currentCol] = ' ';
                    txtGraphics.putString(currentCol, currentRow, " ");
                    // Sets where the next element should come
                    if (ateApple && element == snake.size() - 1) {
                        ArrayList<Integer> grow = new ArrayList<>();
                        moves = 0;
                        if (fruitNumber < 5) {
                            fruitNumber = fruitNumber + 1;
                        }
                        grow.add(currentRow);
                        grow.add(currentCol);
                        snake.add(grow);
                        board[currentRow][currentCol] = '*';
                        txtGraphics.putString(currentCol, currentRow, "*");
                        // Checks what fruit should spawn - pear/apple
                        if (fruitNumber == 5) {
                            fruitCoordinates = fruitGenerator.createFruit(boardHeight, boardWidth, board, false);
                            fruitNumber = 0;
                        } else {
                            fruitCoordinates = fruitGenerator.createFruit(boardHeight, boardWidth, board, true);
                        }
                        ateApple = false;
                    }
                    previousRow = currentRow;
                    previousCol = currentCol;
                }
                // Checks if the snake ate a pear
                if (atePear) {
                    fruitCoordinates = fruitGenerator.createFruit(boardHeight, boardWidth, board, true);
                    moves = 0;
                    atePear = false;
                    /* Checks which direction the tail was pointing, so it can start moving in that direction.
                    It won't allow to hit a wall if the tail points towards it as well*/
                    if (board[snake.get(snake.size() - 1).get(0) - 1][snake.get(snake.size() - 1).get(1)] == '*'
                            || (board[snake.get(snake.size() - 1).get(0) - 1][snake.get(snake.size() - 1).get(1)] == '#'
                            && board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) - 1] == '#'
                            && board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) + 1] == '#'
                            && board[snake.get(snake.size() - 1).get(0) + 1][snake.get(snake.size() - 1).get(1)] == ' ')) {
                        up = false;
                        down = true;
                        right = false;
                        left = false;
                    } else if (board[snake.get(snake.size() - 1).get(0) + 1][snake.get(snake.size() - 1).get(1)] == '*'
                            || (board[snake.get(snake.size() - 1).get(0) + 1][snake.get(snake.size() - 1).get(1)] == '#'
                            && board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) - 1] == '#'
                            && board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) + 1] == '#'
                            && board[snake.get(snake.size() - 1).get(0) - 1][snake.get(snake.size() - 1).get(1)] == ' ')) {
                        up = true;
                        down = false;
                        right = false;
                        left = false;
                    } else if (board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) - 1] == '*'
                            || (board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) - 1] == '#'
                            && board[snake.get(snake.size() - 1).get(0) - 1][snake.get(snake.size() - 1).get(1)] == '#'
                            && board[snake.get(snake.size() - 1).get(0) + 1][snake.get(snake.size() - 1).get(1)] == '#'
                            && board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) + 1] == ' ')) {
                        up = false;
                        down = false;
                        right = true;
                        left = false;
                    } else if (board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) + 1] == '*'
                            || (board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) + 1] == '#'
                            && board[snake.get(snake.size() - 1).get(0) - 1][snake.get(snake.size() - 1).get(1)] == '#'
                            && board[snake.get(snake.size() - 1).get(0) + 1][snake.get(snake.size() - 1).get(1)] == '#'
                            && board[snake.get(snake.size() - 1).get(0)][snake.get(snake.size() - 1).get(1) - 1] == ' ')) {
                        up = false;
                        down = false;
                        right = false;
                        left = true;
                    }
                    Collections.reverse(snake);
                }

                screen.refresh();
            }
        } catch (IOException |
                InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public boolean checkValidMove(int row, int col, char[][] board) {
        return board[row][col] != '#' && board[row][col] != '*';
    }

    public boolean checkForApple(int row, int col, char[][] board) {
        return board[row][col] == 'o';
    }

    public boolean checkForPear(int row, int col, char[][] board) {
        return board[row][col] == 'd';
    }
}

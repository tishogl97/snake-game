package contracts;

public interface FruitGenerator {
    int[] createFruit(int maxRows, int maxCols, char[][] board, boolean isApple);

    void removeFruit(int row, int col, char[][] board);
}

package contracts;

import java.io.FileNotFoundException;

public interface BoardGenerator {

    char[][] boardGeneration() throws FileNotFoundException;
}

package contracts;

import java.util.ArrayList;

public interface SnakeGenerator {

    ArrayList<ArrayList<Integer>> createSnake();

    void displaySnake();
}

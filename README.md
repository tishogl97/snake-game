# A17-Tihomir-Lambov

Tihomir Lambov - Snake Game


How to run the project:

1. Clone this repository on your machine.

2. To run the project you need IDE.
Example: IntelliJ - This is the one used to create the project.

3. Run the application.

4. A text will pop up in the console
You have to select the board you want to play (as there is only one board for now - board1)

5. Write the name of the board in the console ("board1") and a Terminal will open.

6. After you press any arrow key the snake will appear and move the direction you chose.

7. You can navigate the snake using the arrow keys of your keyboard.

8. The rest is easy - eat apples to grow and pears to reverse your snake.
MOST IMPORTANTLY - AVOID WALLS!

